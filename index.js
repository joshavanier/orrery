const c = document.getElementById('c');
const ctx = c.getContext('2d');
const TAU = Math.PI * 2;
const W = 600;
const H = W / 2;

const toRadians = n => n * 0.01745;
const calcSpeed = n => (n === 1 ? 1 : 1 / (n / 365.25));
const calcSize = n => n * 0.67;

function Body (obj) {
  this.id = obj.id;
  this.tether = obj.tether;
  this.r = calcSize(obj.r || 1);
  this.v = calcSpeed(obj.v || 1);
  this.or = calcSize(obj.or || 1);
  this.rs = calcSpeed(obj.rs || 1);

  this.degrees = 0;
  this.x = 0;
  this.y = 0;
  this.vx = 0;
  this.vy = 0;
  this.od = 0;
  this.ox = 0;
  this.oy = 0;

  this.calc = function () {
    const angle = toRadians(this.degrees);
    const oa = toRadians(this.od);
    this.degrees += this.v;
    this.od += this.rs;
    this.vx = this.or * Math.cos(angle);
    this.vy = this.or * Math.sin(angle);
    if (this.tether !== null) {
      this.x = this.vx + this.tether.x;
      this.y = this.vy + this.tether.y;
      this.ox = this.x + Math.cos(oa) * this.r;
      this.oy = this.y + Math.sin(oa) * this.r;
    }
  };
}

function Sys () {
  this.x = 0;
  this.y = 0;
  this.bodies = [];
  this.cache = {};
  this.count = 0;

  this.spawn = function (obj) {
    const o = obj;
    o.tether = o.tether === null ? this : this.cache[o.tether];
    const body = new Body(o);
    body.calc();
    this.bodies[this.bodies.length] = body;
    this.cache[o.id] = body;
    this.count++;
  };

  this.calc = function () {
    for (let i = 0; i < this.count; i++) {
      this.bodies[i].calc();
    }
  };
}

const S = new Sys();
S.x = H;
S.y = H;

function addMoons (dataset, tether, rocheLimit, divisor) {
  for (let i = 0; i < dataset.length; i++) {
    const {n, v, r, or} = dataset[i];
    S.spawn({
      id: n, r: r || 0.25, v, or: rocheLimit + ((or / 128852) / 25), tether
    });
  }
}


S.spawn({id: 'Sol', r: 109, v: 0, or: 0, rs: 24.47, tether: null});
S.spawn({id: 'Mercury', tether: 'Sol', r: 0.3829, v: 87.96926, or: 120, rs: 58.6462});
S.spawn({id: 'Venus', tether: 'Sol', r: 0.9499, v: 224.7008, or: 140, rs: 243.018});
S.spawn({id: 'Earth', tether: 'Sol', r: 1, v: 1, or: 160, rs: 1});
S.spawn({id: 'Luna', tether: 'Earth', r: 0.2727, v: 27.321661});
S.spawn({id: 'Mars', tether: 'Sol', r: 0.532, v: 686.97959, or: 180, rs: 1.0259568});
S.spawn({id: 'Ceres', tether: 'Sol', r: 0.075, v: 1680.22107, or: 200, rs: 0.3781});
S.spawn({id: 'Jupiter', tether: 'Sol', r: 11.2, v: 4332.8201, or: 240, rs: 0.41354});
S.spawn({id: 'Saturn', tether: 'Sol', r: 9.14, v: 10755.699, or: 300, rs: 0.44});
S.spawn({id: 'Uranus', tether: 'Sol', r: 3.981, v: 30687.153, or: 350, rs: 0.71833});
S.spawn({id: 'Neptune', tether: 'Sol', r: 3.865, v: 60190.03, or: 390, rs: 0.67125});
S.spawn({id: 'Pluto', tether: 'Sol', r: 0.19, v: 90553.017, or: 420, rs: 6.38768});
S.spawn({id: 'Eris', tether: 'Sol', r: 0.18, v: 203645, or: 440, rs: 1.08});

addMoons(MARS, 'Mars', 4, 1);
addMoons(JUPITER, 'Jupiter', 16, 25);
addMoons(SATURN, 'Saturn', 12, 25);
addMoons(URANUS, 'Uranus', 7, 50);
addMoons(NEPTUNE, 'Neptune', 4, 100);
addMoons(PLUTO, 'Pluto', 2);
addMoons(ERIS, 'Eris', 2);

const n = S.count;
setInterval(() => {
  ctx.fillStyle = '#030303';
  ctx.fillRect(0, 0, W, W);
  S.calc();

  for (let i = 0; i < n; i++) {
    const {x, y, ox, oy, r} = S.bodies[i];
    ctx.beginPath();
    ctx.arc(x, y, r, 0, TAU);
    ctx.lineWidth = 0.5;
    ctx.moveTo(x, y);
    ctx.lineTo(ox, oy);
    ctx.strokeStyle = '#e4e4e4';
    ctx.stroke();
  }
}, 16);
